import React from 'react';
import PropTypes from 'prop-types';
import BEM from 'bem-class-names-builder';
import { withImages } from '../../context/images';
import './styles.css';

const bem = new BEM('description');

const Description = ({ chosenImage }) => chosenImage && (
  <div className={bem.toString()}>
    Title: {chosenImage.title || ' - '}
  </div>
);

Description.propTypes = {
  chosenImage: PropTypes.shape({
    title: PropTypes.string,
  })
};

Description.defaultProps = {
  chosenImage: {},
};

export default withImages(Description);
