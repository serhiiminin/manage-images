import React from 'react';
import PropTypes from 'prop-types';
import { Dialog } from '@material-ui/core';
import { withImages } from '../../context/images';

const PreviewImage = ({ chosenImage, isViewing, toggleIsViewing }) =>
  chosenImage && (
    <Dialog open={isViewing} onClose={toggleIsViewing}>
      <img src={chosenImage.img} alt={chosenImage.title || chosenImage.src}/>
    </Dialog>
  );

PreviewImage.propTypes = {
  chosenImage: PropTypes.shape({
    id: PropTypes.string,
    img: PropTypes.string,
    title: PropTypes.string,
  }),
  isViewing: PropTypes.bool,
  toggleIsViewing: PropTypes.func.isRequired,
};

PreviewImage.defaultProps = {
  chosenImage: {},
  isViewing: false,
};

export default withImages(PreviewImage);
