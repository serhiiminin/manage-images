import React from 'react';
import PropTypes from 'prop-types';
import BEM from 'bem-class-names-builder';
import { Delete, Edit, Visibility } from '@material-ui/icons';
import { withImages } from '../../context/images';
import { ButtonMini, UploadButton } from '../../components';
import './styles.css';

const bem = new BEM('controls-panel');

const ControlsPanel = ({ uploadImages, deleteImage, toggleIsViewing, toggleIsEditing, chosenImage }) => (
  <div className={bem.toString()}>
    <UploadButton
      title='Upload'
      upload={uploadImages}
    />
    <ButtonMini
      disabled={!chosenImage}
      onClick={toggleIsViewing}
      title='Preview'
    >
      <Visibility/>
    </ButtonMini>

    <ButtonMini
      disabled={!chosenImage}
      onClick={toggleIsEditing}
      title='Edit'
    >
      <Edit/>
    </ButtonMini>
    <ButtonMini
      disabled={!chosenImage}
      onClick={() => deleteImage(chosenImage.id)}
      title='Delete'
    >
      <Delete/>
    </ButtonMini>
  </div>
);

ControlsPanel.propTypes = {
  uploadImages: PropTypes.func.isRequired,
  deleteImage: PropTypes.func.isRequired,
  toggleIsViewing: PropTypes.func.isRequired,
  toggleIsEditing: PropTypes.func.isRequired,
  chosenImage: PropTypes.shape({
    id: PropTypes.string,
    img: PropTypes.string,
    title: PropTypes.string,
  })
};

ControlsPanel.defaultProps = {
  chosenImage: null,
};

export default withImages(ControlsPanel);
