import React from 'react';
import PropTypes from 'prop-types';
import BEM from 'bem-class-names-builder';
import { withImages } from '../../context/images';
import { Image } from '../../components';
import './styles.css';

const bem = new BEM('images-list');

const ImagesList = ({ images, chooseImage, chosenImage }) =>
  images.length > 0
    ? (
      <div className={bem.toString()}>
        {images.map(({ img, title, id }) => (
          <Image
            key={img}
            src={img}
            title={title}
            onClick={() => chooseImage(id)}
            isChosen={chosenImage && chosenImage.id === id}
          />
        ))}
      </div>
    )
    : (
      <div className={bem.elem('empty').toString()}>No images</div>
    );

ImagesList.propTypes = {
  chooseImage: PropTypes.func.isRequired,
  chosenImage: PropTypes.shape({
    id: PropTypes.string,
    img: PropTypes.string,
    title: PropTypes.string,
  }),
  images: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    img: PropTypes.string,
    title: PropTypes.string,
  }))
};

ImagesList.defaultProps = {
  chosenImage: null,
  images: [],
};

export default withImages(ImagesList);
