import React from 'react';
import PropTypes from 'prop-types';
import BEM from 'bem-class-names-builder';
import { TextField } from '@material-ui/core';
import { Check } from '@material-ui/icons';
import { withImages } from '../../context/images';
import { ButtonMini } from '../../components';
import './styles.css';

const bem = new BEM('input-with-submit');

const InputWithSubmit = ({ updateImage, chosenImage, onTitleChange, titleValue, isEditing }) =>
  isEditing && (
    <div className={bem.toString()}>
      <TextField
        autoFocus
        label="Title"
        value={titleValue || ''}
        onChange={event => onTitleChange(event.target.value)}
        disabled={!chosenImage}
        onKeyPress={event => {
          if (event.key === 'Enter') {
            updateImage(chosenImage.id);
          }
        }}
      />
      <ButtonMini
        disabled={!chosenImage}
        onClick={() => updateImage(chosenImage.id)}
        title='Update'
      >
        <Check/>
      </ButtonMini>
    </div>
  );

InputWithSubmit.propTypes = {
  updateImage: PropTypes.func.isRequired,
  onTitleChange: PropTypes.func.isRequired,
  chosenImage: PropTypes.shape({
    id: PropTypes.string,
    img: PropTypes.string,
    title: PropTypes.string,
  }),
  titleValue: PropTypes.string,
  isEditing: PropTypes.bool,
};

InputWithSubmit.defaultProps = {
  chosenImage: null,
  isEditing: false,
  titleValue: '',
};

export default withImages(InputWithSubmit);
