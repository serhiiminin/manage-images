import React from 'react';
import PropTypes from 'prop-types';
import { Button } from '@material-ui/core';

const ButtonMini = ({ children, ...restProps }) => (
  <Button
    color='primary'
    variant='fab'
    mini
    {...restProps}
  >
    {children}
  </Button>
);

ButtonMini.propTypes = {
  children: PropTypes.node.isRequired,
};

export default ButtonMini;
