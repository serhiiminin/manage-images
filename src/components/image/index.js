import React from 'react';
import PropTypes from 'prop-types';
import BEM from 'bem-class-names-builder';
import { Tooltip, Typography } from '@material-ui/core';
import './styles.css';

const bem = new BEM('image-block');

const Image = ({ src, title, onClick, isChosen }) => (
  <Tooltip
    className={bem.mods(isChosen && 'chosen').toString()}
    title={<Typography color="inherit">{title || src}</Typography>}
  >
    <span
      tabIndex="0"
      role="button"
      style={{ backgroundImage: `url(${src})` }}
      onClick={onClick}
      onKeyPress={event => {
        if (event.key === 'Enter') {
          onClick();
        }
      }}
    />
  </Tooltip>
);

Image.propTypes = {
  src: PropTypes.string,
  title: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  isChosen: PropTypes.bool,
};

Image.defaultProps = {
  isChosen: false,
  title: '',
  src: '',
};

export default Image;
