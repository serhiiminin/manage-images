import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { CloudUpload } from '@material-ui/icons';
import BEM from 'bem-class-names-builder';
import { ButtonMini } from '..';
import './styles.css';

const bem = new BEM('upload-button');

class UploadButton extends Component {
  static propTypes = {
    upload: PropTypes.func.isRequired,
    title: PropTypes.string,
  };

  static defaultProps = {
    title: '',
  };

  saveRef = ref => { this.imageInput = ref };

  openModalFileUploader = () => this.imageInput.click();

  render() {
    const { upload, title } = this.props;

    return (
      <ButtonMini
        className={bem.toString()}
        onClick={this.openModalFileUploader}
        title={title}
      >
        <CloudUpload />
        <input
          className={bem.elem('input').toString()}
          name="image"
          type="file"
          accept=".jpg, .jpeg, .png"
          multiple
          ref={this.saveRef}
          onChange={upload}
        />
      </ButtonMini>
    );
  }
}

export default UploadButton;
