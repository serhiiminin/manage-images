import React from 'react';
import BEM from 'bem-class-names-builder';
import { ControlsPanel, InputWithSubmit, PreviewImage, Description } from '../../containers';
import './styles.css';

const bem = new BEM('sidebar');

const Sidebar = () => (
  <div className={bem.toString()}>
    <ControlsPanel />
    <InputWithSubmit />
    <Description />
    <PreviewImage/>
  </div>
);

export default Sidebar;
