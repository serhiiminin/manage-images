import React from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core';
import { SnackbarProvider } from 'notistack';
import BEM from 'bem-class-names-builder';
import StateProvider from '../context';
import { Sidebar } from '../components';
import { ImagesList } from '../containers';
import './styles.css';

const bem = new BEM('app');

const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
  },
});

const App = () => (
  <MuiThemeProvider theme={theme}>
    <SnackbarProvider maxSnack={5}>
      <StateProvider>
        <div className={bem.toString()}>
          <Sidebar/>
          <ImagesList/>
        </div>
      </StateProvider>
    </SnackbarProvider>
  </MuiThemeProvider>
);

export default App;
