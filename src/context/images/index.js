import React, { Component, createContext } from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid';
import { withSnackbar } from 'notistack';
import api from '../../api';

const ImagesContext = createContext({});

const FAKE_ID = 1;
const FAKE_BODY = {
  title: 'text',
  body: 'bla bla',
};

class ImagesProviderCmp extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    onPresentSnackbar: PropTypes.func.isRequired,
  };

  state = {
    images: [],
    chosenImage: null,
    title: '',
    isEditing: false,
    isViewing: false,
  };

  handleTitleChange = title => this.setState({ title });

  handleToggleIsViewing = () => this.setState(prevState => ({ isViewing: !prevState.isViewing }));

  handleToggleIsEditing = () =>
    this.setState(prevState => ({
      isEditing: !prevState.isEditing,
      title: prevState.chosenImage
        ? prevState.chosenImage.title
        : '',
    }));

  handleChooseImage = id =>
    this.setState(prevState => ({
      isEditing: false,
      chosenImage: !prevState.chosenImage || prevState.chosenImage.id !== id
        ? prevState.images.find(image => image.id === id)
        : null,
    }));

  fetchImagesList = () =>
    api.getImagesList()
      .catch(() => this.props.onPresentSnackbar('error', 'Failed fetching data.'));

  updateImage = (imageID, body) =>
    api.updateImage(FAKE_ID, body)
      .then(this.fetchImagesList)
      .then(() => this.setState(prevState => ({
        images: prevState.images
          .map(image => image.id === imageID
            ? {
              ...image,
              title: prevState.title,
            }
            : image
          ),
        chosenImage: null,
        isEditing: false,
        title: '',
      })))
      .then(() => this.props.onPresentSnackbar('success', 'Updated successfully.'))
      .catch(() => this.props.onPresentSnackbar('error', 'Failed fetching data.'));

  uploadImages = event => {
    const { files } = event.target;
    const listOfFiles = Object.values(files);

    return Promise.all([
      ...listOfFiles
        .map(() => api.uploadImage(FAKE_BODY))
    ])
      .then(this.fetchImagesList)
      .then(() =>
        this.setState(prevState => ({
          images: [
            ...prevState.images,
            ...listOfFiles
              .map(image => ({
                id: uuid(),
                img: window.URL.createObjectURL(image),
                title: null,
              }))
          ]
        })))
      .then(() => this.props.onPresentSnackbar('success', 'Uploaded successfully.'))
      .catch(() => this.props.onPresentSnackbar('error', 'Failed fetching data.'));
  };

  deleteImage = id =>
    api.deleteImage(FAKE_ID)
      .then(this.fetchImagesList)
      .then(() => this.setState(prevState => ({
        images: prevState.images.filter(image => image.id !== id),
        chosenImage: null,
        isEditing: false,
      })))
      .then(() => this.props.onPresentSnackbar('success', 'Deleted successfully.'))
      .catch(() => this.props.onPresentSnackbar('error', 'Failed to delete.'));

  render() {
    const { images, chosenImage, title, isEditing, isViewing } = this.state;
    const { children } = this.props;

    return (
      <ImagesContext.Provider
        value={{
          images,
          chosenImage,
          titleValue: title,
          isEditing,
          isViewing,
          toggleIsEditing: this.handleToggleIsEditing,
          toggleIsViewing: this.handleToggleIsViewing,
          onTitleChange: this.handleTitleChange,
          updateImage: this.updateImage,
          uploadImages: this.uploadImages,
          chooseImage: this.handleChooseImage,
          deleteImage: this.deleteImage,
        }}
      >
        {children}
      </ImagesContext.Provider>
    );
  }
}

const withImages = Cmp => props =>
  <ImagesContext.Consumer>{value => <Cmp {...value} {...props} />}</ImagesContext.Consumer>;

const ImagesProvider = withSnackbar(ImagesProviderCmp);

export { ImagesProvider, withImages };
