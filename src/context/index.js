import React from 'react';
import PropTypes from 'prop-types';
import { ImagesProvider } from './images';

const StateProvider = ({ children }) => (
  <ImagesProvider>
    {children}
  </ImagesProvider>
);

StateProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export default StateProvider;
