const checkStatus = response => {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  const error = new Error(response.statusText);

  error.response = response;
  throw error;
};

const parseJson = response => response.json();

const createFetchJson = fetcher => params =>
  fetcher(params)
    .then(checkStatus)
    .then(parseJson)
    .catch(error => {
      throw error;
    });

export default createFetchJson;
