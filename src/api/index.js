import { IMAGES_API } from './endpoints';
import createFetchJson from './create-fetcher';

const fetcher = createFetchJson(window.fetch);

const api = {
  getImage: imageId => fetcher(
    `${IMAGES_API}/posts/${imageId}`,
    {
      method: 'GET',
    }),
  getImagesList: () => fetcher(
    `${IMAGES_API}/posts`,
    {
      method: 'GET',
    }),
  uploadImage: body => fetcher(
    `${IMAGES_API}/posts`,
    {
      method: 'POST',
      body,
    }),
  updateImage: (imageId, body) => fetcher(
    `${IMAGES_API}/posts/${imageId}`,
    {
      method: 'PUT',
      body,
    }),
  deleteImage: imageId => fetcher(
    `${IMAGES_API}/posts/${imageId}`,
    {
      method: 'DELETE',
    }),
};

export default api;
